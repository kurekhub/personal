import { Component } from '@angular/core';
import { NxWelcomeComponent } from './nx-welcome.component';
import {MatButton} from "@angular/material/button";

@Component({
  standalone: true,
  imports: [NxWelcomeComponent, MatButton],
  selector: 'sl-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'shopping-list';
}
